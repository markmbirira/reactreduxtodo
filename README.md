# ReactReduxToDo
Demonstrating usage of React + Redux for basic CRUD (Create Read Update Delete)


![demo](/todoTaskImage.png?raw=true "todoTaskImage")

Clone to you machine and __'cd'__ to the __ReactReduxToDo-master__ and 
>  __npm install__

Run the __webpack-dev-server__
>  __npm start__
